import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:webview_flutter/platform_interface.dart';
import 'package:webview_flutter/webview_flutter.dart';

void main() {
  SystemChrome.setPreferredOrientations([
    DeviceOrientation.landscapeLeft,
    DeviceOrientation.landscapeRight
  ]);
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    final url ="http://vietteldmp.vn";
    return Scaffold(
      body:SafeArea(
        child: WebView(
          onWebResourceError: (WebResourceError webviewerrr) {
            print("Domain  : " +webviewerrr.domain.toString());
            print("Description  : " +webviewerrr.description.toString());
            print("ErrorCode  : " +webviewerrr.errorCode.toString());
            print("FailingUrl  : " +webviewerrr.failingUrl.toString());
          },
          initialUrl: url,
          javascriptMode: JavascriptMode.unrestricted,
          onPageFinished: (String url) {
            print('Page finished loading: ' +url );
          },

        ),
      )
    );
  }
}
